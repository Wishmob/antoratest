//non functional code

const { spawnSync } = require('child_process');
const { existsSync, readFileSync, writeFileSync } = require('fs');
const path = require('path');

function convertMarkdownToAsciidoc(filePath) {
  const result = spawnSync('kramdoc', [filePath], { encoding: 'utf8' });
  if (result.error) {
    console.error(`Failed to execute kramdoc: ${result.error.message}`);
    return null;
  }
  if (result.status !== 0) {
    console.error(`kramdoc exited with code ${result.status}: ${result.stderr}`);
    return null;
  }
  return result.stdout;
}

function processMarkdownFiles({ site }) {
  const pages = site.files;
  pages.forEach(page => {
    if (page.src.extname === '.md') {
      const mdFilePath = page.path;
      const adocContent = convertMarkdownToAsciidoc(mdFilePath);
      if (adocContent) {
        const adocFilePath = mdFilePath.replace(/\.md$/, '.adoc');
        writeFileSync(adocFilePath, adocContent);
        page.src.extname = '.adoc';
        page.contents = Buffer.from(adocContent, 'utf8');
      }
    }
  });
}

module.exports.register = function (registry) {
  registry.preprocess(processMarkdownFiles);
};
